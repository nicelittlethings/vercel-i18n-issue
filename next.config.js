const { i18n } = require('./next-i18next.config')

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  i18n,
  rewrites: async () => {
    return [{
      source: '/en/test-en',
      destination: '/test-nl',
      locale: false
    }]
  }
}

module.exports = nextConfig
