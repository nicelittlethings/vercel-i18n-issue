/** @type {import('next-i18next').UserConfig} */
module.exports = {
    i18n: {
        defaultLocale: 'nl',
        locales: ['nl', 'en'],
        localeDetection: false
    },
    localePath:
        typeof window === 'undefined'
            ? require('path').resolve('./public/locales')
            : '/locales',
    // allows hot reload / fast refresh in dev
    reloadOnPrerender: process.env.NODE_ENV === 'development'
}
