import {useTranslation} from "next-i18next";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';


const Page = () => {
    const { t } = useTranslation('common')

    return (<main
        className={`flex min-h-screen flex-col items-center justify-center`}
    >
        <h1>{t('testTranslationKey')}</h1>
    </main>)

}

// @ts-ignore
export const getServerSideProps = async ({locale}) => ({
    props: {
        ...(await serverSideTranslations(locale, ['common']))
    }
});

export default Page
