import { Inter } from 'next/font/google'
import Link from "next/link";

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-center ${inter.className}`}
    >
      <div className={'flex flex-col gap-4 border border-red-500 p-4 rounded-3xl'}>
        <Link href={'/test-nl'}>Test NL</Link>
        <Link href={'/en/test-en'}>Test EN</Link>
      </div>
    </main>
  )
}
